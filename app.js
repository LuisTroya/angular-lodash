angular.module('app', [])

.controller('AppCtrl', ['$http', '$q', '$scope', AppCtrl])

.controller('FilterKeyCtrl', ['$http', '$q', '$scope', FilterKeyCtrl]);

function FilterKeyCtrl($http, $q, $scope) {
	$scope.filterSelect = 'id';

	$scope.getDataFiltered = function (key) {
		
		$http.get('basic.json').success(function(data) {
			console.log(data.results);

			$scope.datos = _.map(data.results, _.property(key));
			
			console.log( $scope.datos );

		});	
	}


}

function AppCtrl($http, $q, $scope) {
	
	$http.get('data.json').success(function(data) {
		console.log(data);
		// console.log('idSala ->', data.idSala);
		// console.log('tipoSala ->', data.tipoSala);
		// console.log('nombreSala ->', data.nombreSala);
		
		// Obtener objecto -> listaUsuariosSala(siempre sera un arreglo de una position) -> mensajesUsuario(array)
		mensajesUsuarios = data.listaUsuariosSala[0].mensajesUsuario;
		
		// Siempre sera un array de 1 posicion con 'X' objectos dentro
		console.log('listaUsuariosSala ->', data.listaUsuariosSala);
		
		// Imprimir el los mensajes de los usuarios.
		$scope.mensajes = _.map(mensajesUsuarios, _.property('mensaje'));
		console.log( $scope.mensajes );

		// console.log('mensajesUsuario ->', data.mensajesUsuario);

	});	

}